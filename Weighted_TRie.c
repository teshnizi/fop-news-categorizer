#include<stdio.h>
#include<string.h>
#include<stdbool.h>

#define max_word_size 150
#define max_freq 4000
#define min_freq 5

typedef struct trie_node{
    struct tire_node *child[28];
    bool ex;
    int num;
    char key;
    int cat;
    int curent;
}trie;

trie *root_list[50];
char root_name[50][20];
int num_of_news[50],num_of_words[50];
int cat_num;

int cmp(char a[], char b[]){  //compares 2 strings
    int i=0;
    while(i<strlen(a) && i<strlen(b)){
        if(a[i]<b[i])
            return 2;
        if(a[i]>b[i])
            return 1;
        i++;
    }
    if(i<strlen(a))
        return 1;
    if(i<strlen(b))
        return 2;
    return 0;
}

void pre_is_word(trie *node,char a,int news){
    int i;
    for(i=0;i<28;i++)
        node->child[i]=NULL;
    node->ex=1;
    node->num=1;
    node->key=a;
    node->cat=1;
    node->curent=news;
}

void pre_not_word(trie *node,char a){
    int i;

    for(i=0;i<28;i++)
        node->child[i]=NULL;
    node->ex=0;
    node->num=0;
    node->key=a;
}

void kalibrate(char a[]){
    int i;
    for(i=0;i<strlen(a);i++)
    {
       if('A'<=a[i]&&a[i]<='Z')
        a[i]=(char)(a[i]-'A'+'a');
    }
}

void insert(trie  *root,char a[],int news){
    int sz=strlen(a);
    trie *node=root;
    kalibrate(a);
    int i,j;
    for(i=0;i<sz;i++)
    {
        if('a'<=a[i]&&a[i]<='z')
            j=a[i]-'a';
        else if(a[i]=='.')
            j=26;
        else
            j=27;
        if(node->child[j]==NULL)
        {
            node->child[j]=malloc(1*sizeof(trie));
            if(i<sz-1)
                pre_not_word(node->child[j],a[i]);
            else
                pre_is_word(node->child[j],a[i],news);
            node=node->child[j];
        }
        else
        {
            if(i==(sz-1))
            {
               node=node->child[j];
               node->ex=1;
               node->num++;
               if(node->curent!=news)
               {
                    node->cat++;
                    node->curent=news;
               }
                break;
            }
            node=node->child[j];
        }
    }
}

int find(trie *root ,char a[]){
    int sz=strlen(a);
    trie *node=root;
    kalibrate(a);
    int i,j;
    for(i=0;i<sz;i++)
    {

        if('a'<=a[i]&&a[i]<='z')
            j=a[i]-'a';
        else if(a[i]=='.')
            j=26;
        else
            j=27;
        if(node->child[j]!=NULL)
        {
            node=node->child[j];
            if(i==sz-1)
            {
                if(node->ex==1)
                    return node->num;
                return 0;

            }
        }
        else
        return 0;
    }

}

int find_cat_num(trie *root ,char a[]){
    int sz=strlen(a);
    trie *node=root;
    kalibrate(a);
    int i,j;
    for(i=0;i<sz;i++)
    {

        if('a'<=a[i]&&a[i]<='z')
            j=a[i]-'a';
        else if(a[i]=='.')
            j=26;
        else
            j=27;
        if(node->child[j]!=NULL)
        {
            node=node->child[j];
            if(i==sz-1)
            {
                if(node->ex==1)
                    return node->cat;
                return 0;

            }
        }
        else
        return 0;
    }

}
int news;
void insert_all_words(){
    FILE *f=fopen("news_categorized.txt","r");
    char text[1001],cat[170],word[20];
    int it=0,i;
    while(fgets(text,1000,f)!=EOF){

            if(text[0]=='E' && text[1]=='O' && text[2]=='F')
                break;
            fgets(text,1000,f);
            if(text[0]=='E' && text[1]=='O' && text[2]=='F' && text[3]==10)
                return;
                news++;
            fgets(cat,170,f);
              while(cat[1]!='c' || cat[9]=='z')
                fgets(cat,170,f);
   // printf("asd");
                    int sz = strlen(text);
                    it=10;
                    while(2>1){
                        int k=0;
                        while(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.')
                            word[k++]=text[it++];
                        if(word[k-1]=='.')k--;
                        word[k]='\0';
                        insert(root_list[0],word,news);
                        while(it<sz&&!(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.'))
                            it++;
                        if(it>=sz)break;
                    }
                    it=17;
                    fgets(text,1000,f);
                    sz=strlen(text);
                    while(2>1){
                        int k=0;
                        while(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.')
                            word[k++]=text[it++];
                        if(word[k-1]=='.')k--;
                        word[k]='\0';
                        insert(root_list[0],word,news);
                        while(it<sz&&!(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.'))
                            it++;
                        if(it>=sz)
                            break;
                    }
            fgets(text,1000,f);
            if(text[0]=='E' && text[1]=='O' && text[2]=='F' && text[3]==10)
                return;
    }
    fclose(f);
}

void insert_all_words_to_cat(){
    FILE *f=fopen("news_categorized.txt","r");
    char text[1000],cat[170],word[max_word_size];
    int it=0,i;
    while(fgets(text,1000,f)!=EOF){
            if(text[0]=='E' && text[1]=='O' && text[2]=='F' && text[3]==10)
                return;
            fgets(text,1000,f);
            if(text[0]=='E' && text[1]=='O' && text[2]=='F' && text[3]==10)
                return;
            fgets(cat,170,f);
            while(cat[1]!='c' || cat[9]=='z')
                fgets(cat,170,f);
            i=0;it=0;
            while(cat[i]!='[')i++;
            i+=2;
            while(cat[i]!='"')
                cat[it++]=cat[i++];
            cat[it]='\0';
            int found = 0;
            for(i=1;i<=cat_num;i++)
                if(!cmp(cat,root_name[i]))
                {
                    num_of_news[i]++;
                    int sz = strlen(text);
                    it=10;
                    while(2>1){
                        int k=0;
                        while(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.')
                            word[k++]=text[it++];
                        if(word[k-1]=='.')k--;
                        word[k]='\0';
                        int k1=find(root_list[0],word);
                        if(k1>=min_freq&&k1<=max_freq){
                                insert(root_list[i],word,num_of_news[i]);
                                num_of_words[i]++;
                        }
                        while(!(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.'))
                            it++;
                        if(it>=sz)break;
                    }
                    it=17;
                    fgets(text,1000,f);
                    sz=strlen(text);
                    while(2>1){
                        int k=0;
                        while(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.')
                            word[k++]=text[it++];
                        if(word[k-1]=='.')k--;
                        word[k]='\0';
                        int k1=find(root_list[0],word);
                        if(k1>=min_freq&&k1<=max_freq){
                            insert(root_list[i],word,num_of_news[i]);
                            num_of_words[i]++;
                        }
                        while(!(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.'))
                            it++;
                        if(it>=sz)break;
                    }
                    found = 1;
                    break;
                }
            if(found==0){
                cat_num++;
                num_of_news[cat_num]++;
                int sz = strlen(text);
                root_list[cat_num]=malloc(1*sizeof(trie));
                strcpy(root_name[cat_num],cat);
                pre_not_word(root_list[cat_num],'P');
                it=10;
                while(2>1){
                    int k=0;
                    while(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.')
                        word[k++]=text[it++];
                    if(word[k-1]=='.')k--;
                    word[k]='\0';
                    int k1=find(root_list[0],word);
                        if(k1>=min_freq&&k1<=max_freq){
                            insert(root_list[cat_num],word,num_of_news[cat_num]);
                            num_of_words[i]++;
                        }
                    while(!(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.'))
                        it++;
                    if(it>=sz)break;
                }
                it=17;
                fgets(text,1000,f);
                sz=strlen(text);
                while(2>1){
                    int k=0;
                    //printf(":");
                    while(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.')
                        word[k++]=text[it++];
                        //printf(":");
                    if(word[k-1]=='.')k--;
                    word[k]='\0';
                       int k1=find(root_list[0],word);
                        if(k1>=min_freq&&k1<=max_freq){
                            insert(root_list[cat_num],word,num_of_news[cat_num]);
                            num_of_words[i]++;
                        }
                    while(!(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.'))
                        it++;
                    if(it>=sz)break;
                }
            }
            if(fgets(text,1000,f)=="EOF")break;
    }
    fclose(f);
}
//int main()
//{
//    cat_num = 0;
//    root_list[cat_num]=calloc(1,sizeof(trie));
//    strcpy(root_name[cat_num],"all_word");
//    pre_not_word(root_list[cat_num],"P");
//    insert_all_words();
//    insert_all_words_to_cat();
//    printf("%d %d",find_cat_num(root_list[6],"football"),find(root_list[6],"football"));
//}
