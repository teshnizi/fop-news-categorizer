#include <stdio.h>
#include <string.h>
#include<math.h>
#include "Weighted_TRie.c"

#define max_word_size 150
double  make_word_prob(int num,int num_cat,int num_cat_word)
{
    double num1=(double)num,num_cat1=(double)num_cat,num_cat_word1=(double)num_cat_word;
    if(num_cat_word==0)
        return 0;
    return (num1*(log((double)num_cat1/num_cat_word1)));
}
int a=0 ,num_news=0;
trie *news_root[70];
int word_num;


void insert_news(){
    insert_all_words();
    insert_all_words_to_cat();
    printf("Trie trees of categorized news made!\n\nCategorizing news...\n");
    FILE *f=fopen("news_uncategorized.txt","r");
    char text[3000],cat[170],word[max_word_size];
    int it=0,i;
    while(fgets(text,3000,f)!=EOF){

        if(text[0]=='E' && text[1]=='O' && text[2]=='F' && text[3]==10)
            return;
        fgets(text,3000,f);
        if(text[0]=='E' && text[1]=='O' && text[2]=='F' && text[3]==10)
            return;
        num_news++;
        fgets(cat,170,f);
        while(cat[1]!='c' || cat[9]=='z')
            fgets(cat,170,f);
        i=0;it=0;
        while(cat[i]!='[')i++;
        i+=2;
        while(cat[i]!='"')
            cat[it++]=cat[i++];
        cat[it]='\0';
        int sz = strlen(text);
        it=10;
        news_root[num_news]=calloc(1,sizeof(trie));
        pre_not_word(news_root[num_news],"P");
        while(2>1){
            int k=0;
            while(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.')
                word[k++]=text[it++];
            if(word[k-1]=='.')k--;
            word[k]='\0';
            insert(news_root[num_news],word,0);
            insert(news_root[0],word,num_news);
            //printf("%s ",word);
            while(!(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.'))
                it++;
            if(it>=sz)break;
        }
        //it=17;
        fgets(text,3000,f);
        sz=strlen(text);
        while(2>1){
            int k=0;
            while(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.')
                word[k++]=text[it++];
            if(word[k-1]=='.')k--;
            word[k]='\0';
            insert(news_root[num_news],word,0);
            insert(news_root[0],word,num_news);
            while(!(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.'))
                it++;
            if(it>=sz)break;
        }
        int max =1;
    }
    fclose(f);
}


void categorize_news(){
    FILE *f=fopen("news_uncategorized.txt","r");
    FILE *output=fopen("final_output.txt","w");
    char text[3000],cat[170],word[max_word_size];
    int it=0,i;
    fprintf(output,"\n");
    while(fgets(text,3000,f)!=EOF){
        if(text[0]=='E' && text[1]=='O' && text[2]=='F' && text[3]==10)
            break;
        text[strlen(text)-1]='\0';
        fprintf(output,text);
        fgets(text,3000,f);
        text[strlen(text)-1]='\0';
        fprintf(output,text);
        if(text[0]=='E' && text[1]=='O' && text[2]=='F' && text[3]==10)
            break;
        a++;
        fgets(cat,170,f);
        while(cat[1]!='c' || cat[9]=='z'){
            cat[strlen(cat)-1]='\0';
            fprintf(output,cat);
            fgets(cat,170,f);
        }
        i=0;it=0;
        while(cat[i]!='[')i++;
        i+=2;
        while(cat[i]!='"')
            cat[it++]=cat[i++];
        cat[it]='\0';
        double val[17],val1[17],val2[17],val3[17];
        for(i=0;i<=cat_num;i++)
           {
            val[i]=0;
            val1[i]=0;
            val2[i]=0;
            val3[i]=0;
           }
           word_num=0;
        int sz = strlen(text);
        it=10;
         int t=0;
        while(2>1){
            int k=0;
            while(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.')
                word[k++]=text[it++];
            if(word[k-1]=='.')k--;
            word[k]='\0';
            word_num++;
            //printf("%s ",word);
            t=0;
            for(i=1;i<=cat_num;i++)
                if(find(root_list[i],word))
                t++;
            double cat_prob,news_prob;
            news_prob=make_word_prob(find(news_root[a],word),15000,find_cat_num(root_list[0],word));
            for(i=1;i<=cat_num;i++)
               {
                    cat_prob=make_word_prob(find(root_list[i],word),15000,find_cat_num(root_list[0],word));
                    val3[i]+=cat_prob*cat_prob;
                    val[i]+=cat_prob*news_prob;
                    val2[i]+=news_prob*news_prob;
                }
            while(!(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.'))
                it++;
            if(it>=sz)break;
        }
        //it=17;
        fgets(text,3000,f);
        sz=strlen(text);
        t=0;
//        for(i=1;i<=cat_num;i++)
//            val[i]/=word_num;
        word_num=0;
        while(2>1){
            int k=0;
            while(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.')
                word[k++]=text[it++];
            if(word[k-1]=='.')k--;
            word[k]='\0';
            word_num++;
             double cat_prob,news_prob;
                     t=0;
            for(i=1;i<=cat_num;i++)
                if(find(root_list[i],word))
                t++;
              news_prob=make_word_prob(find(news_root[a],word),15000,find_cat_num(root_list[0],word));
            for(i=1;i<=cat_num;i++)
            {
                cat_prob=make_word_prob(find(root_list[i],word),15000,find_cat_num(root_list[0],word));
                val1[i]+=cat_prob*news_prob;
                val3[i]+=cat_prob*cat_prob;
                val2[i]+=news_prob*news_prob;
            }
            while(!(('a'<=text[it] && text[it]<='z')||('A'<=text[it]&&text[it]<='Z')||text[it]=='.'))
                it++;
            if(it>=sz)break;
        }
        int max =1,max2 =2;
        //printf("\n");
            for(i=1;i<=cat_num;i++)
                val3[i]=sqrt(val3[i]);
             for(i=1;i<=cat_num;i++)
                val[i]+=val1[i];
            for(i=1;i<=cat_num;i++)
        val2[i]=sqrt(val2[i]);
        for(i=1;i<=cat_num;i++)
            val[i]=val[i]/(val2[i]*val3[i]);

        for(i=1;i<=cat_num;i++){
               //printf("  %s : %lf",root_name[i],val[i]);
                if(val[i]>val[max])max = i;
        }
        for(i=1;i<=cat_num;i++)
                if(val[i]>val[max2] && i!=max)max2 = i;

//        printf("\nCat is: %s ",root_name[max]);
//        if(max2!=max && val[max2]>= 0.95*val[max])
//            printf("and %s",root_name[max2]);
//        printf("\n\n");
        //system("pause");
        fprintf(output,"\"categories\" : [\"");
        fprintf(output,root_name[max]);
        fprintf(output,"\"],");
        text[strlen(text)-1]='\0';
        fprintf(output,text);
        fprintf(output,"}\n");
        if(fgets(text,3000,f)=="EOF")break;
    }
    fprintf(output,"EOF\n");
    fclose(f);
    fclose(output);
}
