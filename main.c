/*
    file assortment project
    team12
    team name: 2FUN
    member 1 : Ahmad Agapoor
    member 2 : Ali Ahmadi
*/

#include <stdio.h>
#include <string.h>
#include <curl/curl.h>
#include "transmitter.c"
#include "categorizer.c"
//#include "Weighted_TRie.c"
int find_string_in_string_help(char a[],char b[],int index){
    int i,sz=strlen(a);
    if(strlen(b)-index < sz)
        return 0;
    for(i=0;i<sz;i++)
        if(b[index+i]!=a[i])
            return 0;
   return 1;
}

int find_string_in_string(char a[],char b[],int start){
    int i,sz=strlen(b);
    for(i=start;i<sz;i++)
        if(find_string_in_string_help(a,b,i)==1)
            return i;
    return -1;
}

char* extract_from_text(char a[]){

    int i=0,it=0;

    //extracting title:
    if(find_string_in_string_help("<ti",a,0)){
//        strcpy(ch,"title: ");
        char *ch = malloc(strlen(a)*sizeof(char));
        ch[0]='"',ch[1]='t',ch[2]='i',ch[3]='t',ch[4]='l',ch[5]='e',ch[6]='"',ch[7]=':',ch[8]=' ';
        ch[9]='"';
        i = 7;it = 10;
        while(2>1)
        {
            if(find_string_in_string_help("</ti",a,i)==1)
                {
                    ch[it]='"';
                    it++;
                    break;
                }
            if(find_string_in_string_help("&#",a,i)==1){
                while(find_string_in_string_help(";",a,i)==0)
                    i++;}
                    if(a[i]==';')
                        ch[it]='�';
                    else
            ch[it]=a[i];
            i++;
            it++;
        }
        ch[it]='\0';
        return ch;
    }
    //extracting pubDate:
    else if(find_string_in_string_help("<pu",a,0)){
             char *ch = malloc(strlen(a)*sizeof(char));
        ch[0]='"',ch[1]='d',ch[2]='a',ch[3]='t',ch[4]='e',ch[5]='"',ch[6]=':',ch[7]=' ',ch[8]='"';
        i = 9;it = 9;
        while(2>1)
        {
            if(find_string_in_string_help("</pu",a,i)==1)
                {
                    ch[it]='"';
                    it++;
                    break;
                }
            if(find_string_in_string_help("&#",a,i)==1){
                while(find_string_in_string_help(";",a,i)==0)
                    i++;}
            ch[it]=a[i];
            i++;
            it++;
        }
        ch[it]='\0';
        return ch;
    }

    //extracting link:
    else if(find_string_in_string_help("<li",a,0)){
             char *ch = malloc(strlen(a)*sizeof(char));
        ch[0]='"',ch[1]='l',ch[2]='i',ch[3]='n',ch[4]='k',ch[5]='"',ch[6]=':',ch[7]='"';
        i = 6;it = 8;
        while(2>1)
        {
            if(find_string_in_string_help("</li",a,i)==1)
                {
                    ch[it]='"';
                    it++;
                    break;
                }
            if(find_string_in_string_help("&#",a,i+34)==1){
                while(find_string_in_string_help(";",a,i+34)==0)
                    i++;}
            ch[it]=a[i];
            i++;
            it++;
        }
        ch[it]='\0';
        return ch;
    }

    //extracting date:
    else if(find_string_in_string_help("<des",a,0)){
             char *ch = malloc(strlen(a)*sizeof(char));
        ch[0]='"',ch[1]='d',ch[2]='e',ch[3]='s',ch[4]='c',ch[5]='r',ch[6]='i',ch[7]='p',ch[8]='t',ch[9]='i',ch[10]='o',ch[11]='n',ch[12]='"',ch[13]=':',ch[14]=' ',ch[15]='"';
        i = 0;
        it = 16;
        while(a[i]!='C')i++;
        //printf("%c\n",a[i]);

        while(a[i]!='[')i++;
        i++;
        if(a[i]=='<'){
            if(a[i+1]=='p')
                i+=3;
            else
                while(a[i]!='>')i++;
        }
        while(a[i]!='>' && a[i]!=']'&&i<strlen(a)){
                //printf("%c",a[i]);//system("pause");
//                if(a[i]=='<'){
//                        if(a[i+1]=='p')
//                            i+=3;
//                        else{
//                            while(a[i]!='>')i++;
//                            i++;
//                        }
//                }
                ch[it++]=a[i++];
        }
        ch[it]='"';
        it++;
        ch[it]='\0';
        return ch;
    }
    else if(find_string_in_string_help("<ca",a,0)){
             char *ch = malloc(strlen(a)*sizeof(char));
        strcpy(ch,"category: ");
        i = 10;it = 10;
        while(a[i]!='C')
            i++;
        while(a[i]!='[')i++;
            i++;
        if(a[i]==' ')i++;
        while(a[i]!=']' && a[i]!=' ')
            ch[it++]=a[i++];
        ch[it]='\0';
        return ch;
    }
    else
        {

             char *ch = malloc(strlen(a)*sizeof(char));
            ch[0]='\0';
            return ch;

        }

}

void extract_RSS(char from[], char to[]){
    FILE* RSS_file = fopen( from, "r");
    FILE* news_file = fopen( to, "w");
    char tmp[4000];
    int k=0,m=0,has_cat=0;
    while(fgets(tmp,4000,RSS_file)!=NULL){

        char *tmp2 = tmp;
        while(tmp2[0]==9)  // shifting spaces
            tmp2++;
        if(find_string_in_string_help("<ite",tmp2,0)==1 || find_string_in_string_help("<\ite",tmp2,0)==1){
            k++;
            if(k%2)
                fprintf(news_file,"{\n");
            else
                fprintf(news_file,"}\n");
        }
        if(k%2==0)
            continue;
        char* ch = extract_from_text(tmp);

        if(ch[strlen(ch)-1]=='\n')
            ch[strlen(ch)-1]=' ';
        if(ch[0]=='c' || ch[0]=='/0')
        {
            if(!has_cat)
            {
                has_cat=1;
                if(from[0]=='u')
                    fprintf(news_file,"\"categorized\" : false,\n");
                else
                    fprintf(news_file,"\"categorized\" : true,\n");
                fprintf(news_file,"\"categories\" : [\"");
                char* temp2 = ch+10;
                fprintf(news_file,"%s\"",temp2);
            }
            else
            {
                 char* temp2 = ch+10;
                 fprintf(news_file,", \"%s\"",temp2);
            }
        }
        else{
            if(has_cat==1)
            {
                fprintf(news_file,"],\n");
                has_cat=0;
            }
           if(ch[0]!='\0')
           {
                fprintf(news_file,ch);
                if(ch[1]=='d'&&ch[2]=='e')
                    fprintf(news_file,"\n");
                else
                     fprintf(news_file,",\n");
            }
        }
        free(ch);
    }
    fprintf(news_file,"}\nEOF\n");
    fclose(RSS_file);
    fclose(news_file);
}

void fix_RSS(char from[], char to[]){
    FILE* RSS_file = fopen( from, "r");
    FILE* fixed_file = fopen( to, "w");
    char *tmp=malloc(4000 * sizeof(char));
    int i=0;
    char *tmp2=tmp;
    while(fgets(tmp,4000,RSS_file)!=NULL){
        if(strlen(tmp)<=2)
            continue;
        while(tmp[0]==9 || tmp[0]==' ')
            tmp++;

//        if(tmp[strlen(tmp)-1]=='\n')
        //printf("%d %d %s",tmp[0],tmp[1],tmp);
        if( tmp[0]=='<' && ((tmp[1]=='l' && tmp[2]=='i') || (tmp[1]=='c' && tmp[2]=='a'))  ){
                //printf("LOL %d %d %s",tmp[0],tmp[1],tmp);
                tmp[strlen(tmp)-1]=' ';
                fprintf(fixed_file,tmp);

                tmp = tmp2;
                fgets(tmp,4000,RSS_file);
                while(tmp[0]==9 || tmp[0]==' ')
                    tmp++;
                tmp[strlen(tmp)-1]=' ';
                fprintf(fixed_file,tmp);


                tmp = tmp2;
                fgets(tmp,4000,RSS_file);
                while(tmp[0]==9 || tmp[0]==' ')
                    tmp++;
                fprintf(fixed_file,tmp);
        }
        //printf("%d %d %s",tmp[0],tmp[1],tmp);
        else if( tmp[0]=='<' && ((tmp[1]=='d' && tmp[2]=='e'))  ){

                tmp[strlen(tmp)-1]=' ';
                fprintf(fixed_file,tmp);

                while(tmp[0]!=']'){
                    tmp = tmp2;
                    fgets(tmp,4000,RSS_file);
                    while(tmp[0]==9 || tmp[0]==' ')
                        tmp++;
                    tmp[strlen(tmp)-1]=' ';
                    fprintf(fixed_file,tmp);
                    //printf("%s\n\n\n",tmp);
                    //system("pause");
                }

                tmp = tmp2;
                fgets(tmp,4000,RSS_file);
                while(tmp[0]==9 || tmp[0]==' ')
                    tmp++;
                fprintf(fixed_file,tmp);
        }
        //printf("%d %d %s",tmp[0],tmp[1],tmp);
        else
            fprintf(fixed_file,tmp);







        tmp = tmp2;
    }
    free(tmp2);
    fclose(RSS_file);
    fclose(fixed_file);
}

int main()
{
    cat_num = 0;
    root_list[cat_num]=calloc(1,sizeof(trie));
    strcpy(root_name[cat_num],"all_word");
    pre_not_word(root_list[cat_num],"P");
    news_root[0]=calloc(1,sizeof(trie));
    pre_not_word(news_root[0],"P");
//    get_URLS_from_server();
    get_uncategorized_URLS_from_server();
//    printf("Categorized news URLs received from server!\n\nGetting news RSS from sites...\n\n");
//    get_RSS_from_URL_list();
    get_uncategorized_RSS_from_URL_list();
    printf("RSS received from URLs!\n");
    fix_RSS("RSS.txt","RSS_fixed.txt");
    extract_RSS("RSS_fixed.txt","news_categorized.txt");
    fix_RSS("uncategorized_RSS.txt","uncategorized_RSS_fixed.txt");
    extract_RSS("uncategorized_RSS_fixed.txt","news_uncategorized.txt");
    printf("Categorized news extracted from RSS!\n\nMaking Trie Trees from news...\n");
    insert_news();
    categorize_news();
    send_to_server();
    printf("News categorized!\n");
}
