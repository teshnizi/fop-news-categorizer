#include <stdio.h>
#include <curl/curl.h>



void get_RSS_from_URL_list(){  //reads word from file and returns root pointer
    //getting files from server:
    FILE* URL_file = fopen( "URL_list.txt", "r");
    FILE* RSS_file = fopen( "RSS.txt", "w");

    char tmp[150];
    while(fscanf(URL_file,"%s",tmp)!=EOF){

        printf("%s\n",tmp);
        int i;
        int sz=strlen(tmp);
        if(tmp[4]=='s')
            for(i=4;i<sz;i++)
                tmp[i]=tmp[i+1];
        CURL *curl;
        CURLcode res;
        curl = curl_easy_init();
        if(curl) {
            curl_easy_setopt(curl, CURLOPT_URL,tmp);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA,RSS_file) ;
            fprintf(RSS_file,"\n\n\n\n");
            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
            res = curl_easy_perform(curl);
            if(res != CURLE_OK)
              fprintf(stderr, "curl_easy_perform() failed: %s\n",
                      curl_easy_strerror(res));
            curl_easy_cleanup(curl);
        }
    }
    fclose(RSS_file);
    fclose(URL_file);
}

void get_URLS_from_server(){  //reads word from file and returns root pointer
    //getting files from server:
    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, "http://fop-project.ir/news/get-urls/?phase=2");
        curl_easy_setopt(curl, CURLOPT_USERPWD, "team12:nimbo2016");
        FILE* file = fopen( "URL_list.txt", "w");
        curl_easy_setopt( curl, CURLOPT_WRITEDATA, file) ;
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        res = curl_easy_perform(curl);
        if(res != CURLE_OK)
          fprintf(stderr, "curl_easy_perform() failed: %s\n",
                  curl_easy_strerror(res));
        curl_easy_cleanup(curl);
        fclose(file);
    }
}


void get_uncategorized_RSS_from_URL_list(){  //reads word from file and returns root pointer
    //getting files from server:
    FILE* URL_file = fopen( "uncategorized_URL_list.txt", "r");
    FILE* RSS_file = fopen( "uncategorized_RSS.txt", "w");

    char tmp[150];
    while(fscanf(URL_file,"%s",tmp)!=EOF){

        printf("%s\n",tmp);
        int i;
        int sz=strlen(tmp);
        if(tmp[4]=='s')
            for(i=4;i<sz;i++)
                tmp[i]=tmp[i+1];
        CURL *curl;
        CURLcode res;
        curl = curl_easy_init();
        if(curl) {
            curl_easy_setopt(curl, CURLOPT_URL,tmp);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA,RSS_file) ;
            fprintf(RSS_file,"\n\n\n\n");
            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
            res = curl_easy_perform(curl);
            if(res != CURLE_OK)
              fprintf(stderr, "curl_easy_perform() failed: %s\n",
                      curl_easy_strerror(res));
            curl_easy_cleanup(curl);
        }
    }
    fclose(RSS_file);
    fclose(URL_file);
}

void get_uncategorized_URLS_from_server(){  //reads word from file and returns root pointer
    //getting files from server:
    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, "http://fop-project.ir/news/get-urls/?phase=3");
        curl_easy_setopt(curl, CURLOPT_USERPWD, "team12:nimbo2016");
        FILE* file = fopen( "uncategorized_URL_list.txt", "w");
        curl_easy_setopt( curl, CURLOPT_WRITEDATA, file) ;
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        res = curl_easy_perform(curl);
        if(res != CURLE_OK)
          fprintf(stderr, "curl_easy_perform() failed: %s\n",
                  curl_easy_strerror(res));
        curl_easy_cleanup(curl);
        fclose(file);
    }
}

void put_in_news_file(char ch[]){

    FILE* news_file = fopen( "raw_news.txt", "w");
    fprintf(news_file,ch);
    fprintf(news_file,"\n\n\n\n");
    fclose(news_file);
}

//void send_to_server(char ans[]){  // sends answer to server and gets the response
//    CURL *curl;
//    CURLcode res;
//    curl_global_init(CURL_GLOBAL_ALL);
//    curl = curl_easy_init();
//    struct curl_slist *list = NULL;
//    if(curl) {
//    list = curl_slist_append(list, "X-Requested-With: XMLHttpRequest");
//    list = curl_slist_append(list, "Content-Type: application/json");
//    curl_easy_setopt(curl, CURLOPT_URL, "http://fop-project.ir/news");
//    curl_easy_setopt(curl, CURLOPT_USERPWD, "team12:nimbo2016");
//    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
//    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, ans);
//    res = curl_easy_perform(curl);
//    if(res != CURLE_OK)
//      fprintf(stderr, "curl_easy_perform() failed: %s\n",
//              curl_easy_strerror(res));
//    curl_easy_cleanup(curl);
//    }
//    curl_global_cleanup();
//    return;
//}

char post[7007];
void send_to_server(){
    //int x=0;
    FILE* news_file = fopen( "final_output.txt", "r");
    fgets(post,7007,news_file);//for blank first line
    while(fgets(post,7007,news_file)!=EOF){
        int i;
        if(post[0]=='E' && post[1]=='O' && post[2]=='F')
            break;
        for(i=0;i<7007;i++)
            if(!( ('a'<=post[i] && post[i] <= 'z')  ||  ('A'<= post[i] && post[i]<='Z')  ||  ('0'<=post[i] && post[i]<='9') || post[i]=='\0' || post[i]==':' || post[i]=='\"' || post[i]=='{' || post[i]=='}' || post[i]==',' || post[i]=='[' || post[i]==']'))
                post[i] = ' ';
        //printf("%s\n",post);
        //printf("%d\n",++x);
        CURL *curl;
        CURLcode res;
        curl = curl_easy_init();
        struct curl_slist *headers = NULL;
        headers = curl_slist_append(headers, "Content-Type: application/json");
        headers = curl_slist_append(headers, "X-Requested-With: XMLHttpRequest");
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER,headers);
        curl_easy_setopt(curl, CURLOPT_URL, "http://team12:nimbo2016@fop-project.ir/news");
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post);
        res = curl_easy_perform(curl);
        printf("\n\n");
        //system("pause");
    }
    fclose(news_file);
}
